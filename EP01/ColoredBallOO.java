import java.awt.Color;
public class ColoredBallOO {
  private Vector pos;
  private Vector vel;       // posição e velocidade
  private final double radius;    // raio
  private final Color color;     // cor

  //constructor
  public ColoredBallOO(Vector p, Vector v, double r, Color c){
    pos = p;
    this.vel = v;
    radius = r;
    color = c;
  }

  public Vector pos()         { return pos;    }
  public Vector vel()         { return vel;    }
  public double radius()      { return radius; }

  public void move(double size, double dt){
    treatWalls(size, dt);
    updatePosition(dt);
  }

  public void setVel(Vector v){ vel = v; }

  public void updatePosition(double dt) {
    pos.cartesian(0) = pos.cartesian(0) + dt*vel.cartesian(0);
    pos.cartesian(1) = pos.cartesian(1) + dt*vel.cartesian(1);
    }

  public void treatWalls(double size, double dt){
    if (pos.cartesian(0) + dt*vel.cartesian(0) > size - radius || pos.cartesian(0) + dt*vel.cartesian(0) < radius - size) vel.cartesian(0) = -vel.cartesian(0);
    if (pos.cartesian(1) + dt*vel.cartesian(1) > size - radius || pos.cartesian(1) + dt*vel.cartesian(1) < radius - size) vel.cartesian(1) = -vel.cartesian(1);
  }

  // draw the ball
  public void draw() {
    StdDraw.setPenColor(color);
    StdDraw.filledCircle(pos, radius);
  }
}
