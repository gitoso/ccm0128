#**CCM0128**
---
####_3 de março de 2020_

##_binary search_
* a busca binária é proporcional a lgN (a busca sequencial, a N²)
* antes da busca é preciso fazer o _sorting_

 >*lgN é uma notação comum para o logaritmo na base 2*

##_sorting_
* bubble sorting não é eficiente
* insertion sorting também cresce com N²
* merge sort (von Neumann)
  1. divide o array em 2
  1. recursivamente ordena as metades
  1. merge
  1. repeat
* merge sort é N lgN
